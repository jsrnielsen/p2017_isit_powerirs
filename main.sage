"""
Test sheet for irs_power.sage
"""

load('./interleaved_code.sage')
load('./irs_power.sage')
load('./failure_prob.sage')
load('./util.sage')


# Code/Decoder Parameters
F = GF(Primes().next(255),'a')
n,k= F.cardinality()-1, floor((F.cardinality()-1)/3)
base_code = codes.GeneralizedReedSolomonCode(F.list()[1:n+1], k, column_multipliers = random_error(n, F, n))

h = 2
IC = IRSCode(base_code, h)

#
c = IC.random_element()
m = IC.unencode(c)
assert c == IC.encode(m)


###

## Optional: Compute good parameters
# print 'computing list of good parameters ...'
# ell_s_list, ell_s_list_minimal = irs_power_decoding_tau_ell_s_list(C,h)

s, ell = 3, 4
Dec = IRSPowerDecoder(IC, (s, ell))
tau_max = Dec.decoding_radius()
tau = tau_max-1 # It seems that there is some slight mistake in the derivation of the decoding radius since tau_max many errors are usually not correctable, but tau_max-1 many errors are

tau = tau_max

# Inform User About Parameters
print '\n###################################################'
print 'Parameters:'
print '###################################################'
print 'Interleaving degree        h =', h
print 'Power Parameter          ell =', ell
print 'Multiplicity Parameter     s =', s
print '###################################################'
print 'Decoding Radius        t_max =', tau_max
print 'Number of Errors         tau =', tau
print '###################################################'
print '[SSB] Decoding Radius        =', floor(h/(h+1)*(n-k))
print '###################################################\n'


### Encoding and Channel
Ch = SynchronizedBurstStaticRateChannel(IC.ambient_space(), tau, IC.order())
c = IC.random_element()
r = Ch.transmit(c)

print Ch.burst_error_distance(r, c)

### A single decoding
print "Running a single decoding:"
m = Dec.decode_to_message(r)
if m == IC.unencode(c):
    print "Decoding succeeded"
else:
    print  "Decoding failed!"


### A longer decoding test
print "Running a longer decoding test:"
report = test_decoder(IC, Dec, Ch, N=100, progress=True)


### For debugging: Explicitly compare the error locator
print '\n###################################################'

v = Dec._short_vector(Dec._build_module(r))

errs = Ch.burst_non_zero_positions(r - c)
alphas = IC.evaluation_points()
x = Dec._Px.gen()
Lambda = prod(x - alphas[i] for i in errs)

if v[0].monic()==Lambda^s:
    print 'Decoding successful!'
else:
    print 'Decoding failed!'
print '###################################################\n'
