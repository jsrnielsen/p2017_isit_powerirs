Implementation of improved Power-decoding of Interleaved Reed-Solomon code
==========================================================================

This is a publically available repository of an implementation which is
connected with the following publication:

* * *
**Decoding of Interleaved Reed--Solomon Codes Using Improved Power Decoding**

by Sven Puchinger and Johan Rosenkilde (né Nielsen)

Submitted to International Symposium in Information Theory 2017 (ISIT)

* * *

The code is written by Sven Puchinger and Johan Rosenkilde.

The code is documented in a self-contained fashion, but it should be
understandable for someone with the article at hand.

* * *

The code is written in [Sage], and will require this to run.
The source file(s) mention the latest version of Sage for which I have verified
they work. The code also requires installation of [Codinglib], Johan
Rosenkilde's library of functions and algorithms for algebraic coding theory.
Before running the code in the files of this repository, one needs to import
Codinglib into the running Sage process:

    from codinglib import *

`main.sage` demonstrates the implementation and is a good entry-point for
reading the code.

This code is released under the GPL v3 or later to conform with Sage's
licensing.

You are welcome to contact me for questions on the implementation or its usage,
or, of course, a discussion of the paper.

Regards,  
Johan Rosenkilde  
jsrn@jsrn.dk  
[jsrn.dk]

[Sage]: http://sagemath.org
[jsrn.dk]: http://jsrn.dk
[Codinglib]: http://jsrn.dk/codinglib
